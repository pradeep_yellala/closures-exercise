

function counterFactory() {
    return {
        count: 0,
        increment: () => {
            this.count++;
        },
        decrement: () => {
            this.count--;
        }
    }
}


module.exports = { counterFactory }