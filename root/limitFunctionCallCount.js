

function limitFunctionCallCount(cb, n) {
    let count = 0
    if (typeof cb !== 'function') {
        return "please provide a function";
    }

    if (!(Number.isInteger(n))) {
        return "please provide second argument as an integer";
    }

    const invokeCbFunc = () => {
        if (count < n) {
            count++
            return cb();
        } else {
            return null;
        }
    }
    return invokeCbFunc;
}


module.exports = { limitFunctionCallCount }