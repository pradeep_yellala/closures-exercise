


function cacheFunction(cb) {
    let cache = {};

    if (cb === undefined || typeof cb !== 'function') {
        return "please provide a function as callback";
    }

    const invokeCbFunc = () => {
        let arg = [...arguments].join(",")
        if (arg in cache) {
            return cache[arg];
        }
        cache[arg] = cb(...arguments);
        return cache[arg];

    }
    return invokeCbFunc;
}


module.exports = { cacheFunction }