

const { limitFunctionCallCount } = require('../root/limitFunctionCallCount');

const limitFunction = limitFunctionCallCount(() => "hello", 4);
console.log(limitFunction());
console.log(limitFunction());
console.log(limitFunction());
console.log(limitFunction());
console.log(limitFunction());