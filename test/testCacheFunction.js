
const { cacheFunction } = require('../root/cacheFunction');


const cacheFunc = cacheFunction((a, b) => a - b);
console.log(cacheFunc(2, 3));
console.log(cacheFunc(2, 2));
console.log(cacheFunc(2, 2));